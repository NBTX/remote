const { version } = require('./package.json');
const WebSocket = require('ws');

/**
 * @type WebSocket.Server[]
 */
const servers = {};

module.exports = {
    /**
     * @function remote
     * @description Create HTTP servers for remote Thornhill OS code execution
     * @param Window
     * @param Ouput
     * @param _
     * @param help
     * @param portToOpen - Port for new server to listen to
     * @param portToClose - Port from existing server to stop listening to
     * @param closeAll - Stop all listening servers
     */
    default: async ({ Window, Ouput }, _, { help, open: portToOpen, close: portToClose, closeAll }) => {
        let res = '';
        if (help) {
            return `remote v${version}`
                + `\nUsage : remote [--open <port>]`
                + `\n        remote [--close <port>]`
                + `\n        remote [--close-all]`;
        }

        if (portToOpen) {
            if (servers[portToOpen])
                res += `Port ${portToOpen} already open.\n`;
            else {
                const wss = new WebSocket.Server({ port: portToOpen });

                wss.on('connection', (ws) => {
                    ws.on('message', (msg) => {
                        const data = JSON.parse(msg);

                        try {
                            const response = eval(data.payload);
                            ws.send(JSON.stringify({
                                error: false,
                                id: data.id,
                                response
                            }));
                        } catch (ex) {
                            ws.send(JSON.stringify({
                                error: true,
                                id: data.id,
                                response: ex
                            }));
                        }
                    })
                });

                servers[portToOpen] = wss;
                res += `Port ${portToOpen} open.\n`;
            }
        }

        if (portToClose) {
            if (servers[portToClose]) {
                await new Promise(resolve => servers[portToClose].close(resolve));
                delete servers[portToClose];
                res += `Port ${portToClose} closed.\n`;
            }
            else res += `Port ${portToClose} wasn't open.\n`;
        }

        if (closeAll) {
            for (const [port, server] of Object.entries(servers)) {
                await new Promise(resolve => server.close(resolve));
                delete servers[port];
                res += `Port ${port} closed.\n`;
            }
        }

        return res.trim();
    }
};